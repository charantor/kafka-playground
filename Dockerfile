FROM node:10-alpine

RUN apk add make gcc g++ python

WORKDIR /app

ENV HOST=0.0.0.0
ENV NODE_ENV=development

COPY ./package*.json ./
RUN npm install

COPY . /app

CMD npm start
