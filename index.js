const express = require('express');

const app = express();
const kafka = require('kafka-node');
const { promisify } = require('util');

const port = process.env.PORT || 3000;
const name = process.env.NAME || 'App';

app.get('/', (req, res) => {
  res.send(`Hello from ${name}`);
});

app.listen(port, '0.0.0.0', () => {
  console.log(`${name} is listening on port ${port}`);
});

const client = new kafka.KafkaClient({ kafkaHost: 'kafka:9092', requestTimeout: 1000 });

const validTopics = [{
  topic: 'universal',
  partitions: 1,
  replicationFactor: 1
}];

const getProducer = (client) => new Promise((resolve, reject) => {
  const producer = new kafka.Producer(client);

  producer.on('ready', function () {
    resolve(producer);
  });

  producer.on('error', (err) => {
    reject(err);
  });
});

(async () => {
  try {
    const producer = await getProducer(client);

    await promisify(producer.createTopics.bind(producer))(validTopics);
    console.log('Created topics', validTopics);


    setTimeout(() => {
        return;
        // publishing messages
      const payloads = [
        {
          topic: 'universal',
          messages: JSON.stringify({ message: 'lol 7' })
        }
      ];
  
      producer.send(payloads, (error, data) => {
        if (error) {
          console.error(`kafka:ElasticsearchProducer: error: ${error}`);
        } else {
          console.log('kafka:ElasticsearchProducer: message published successfully', data);
        }
      });
    }, 3000)

    const options = {
      kafkaHost: 'kafka:9092', // connect directly to kafka broker (instantiates a KafkaClient)
      groupId: 'ExampleTestGroup',
      sessionTimeout: 15000,
      protocol: ['roundrobin'],
      encoding: 'utf8', 
      commitOffsetsOnFirstJoin: true, // on the very first time this consumer group subscribes to a topic, record the offset returned in fromOffset (latest/earliest)
      autoCommit: false
    };
  
    const universalConsumerGroup =  new kafka.ConsumerGroup(options, 'universal');
    
    universalConsumerGroup.on('message', async (msg) => {
      console.log(`${name} got: ${JSON.stringify(msg, null, '  ')}`);
      let obj = {
        topic : msg.topic,
        partition: msg.partition,
        offset: msg.offset
      }

      universalConsumerGroup.commit(obj, (err, data) => {
        if (err) {
          console.error(`${name} got error while committing`, err);
        }
        console.log(`${name} Committed`, data);
      });
      
    });

    universalConsumerGroup.on('error', async(err) => {
      console.error('Error in consumer group', err);
    })
  } catch (err) {
    console.error('ERROR', err);
  }
})();
